/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uo.harish.prakash.sycamore.plugins.measures;

import it.diunipi.volpi.sycamore.engine.Point2D;
import it.diunipi.volpi.sycamore.engine.SycamoreRobot;
import it.diunipi.volpi.sycamore.gui.SycamorePanel;
import it.diunipi.volpi.sycamore.plugins.measures.MeasureImpl;
import java.util.Iterator;
import net.xeoh.plugins.base.annotations.PluginImplementation;

/**
 *
 * @author harry
 */
@PluginImplementation
public class FinalConfiguration extends MeasureImpl {

	private static String	_author				= "Harish Prakash";
	private static String	_shortDescription	= "Get final configuration";
	private static String	_description		= "Get the final configuration of the system";
	private static String	_pluginName			= "FinalConfiguration";

	@Override
	public void onSimulationStart() {}

	@Override
	public void onSimulationStep() {}

	@Override
	public void onSimulationEnd() {

		@SuppressWarnings("unchecked")
		Iterator<SycamoreRobot<?>> iterator = engine.getRobots().iterator();
		while (iterator.hasNext()) {
			SycamoreRobot<?> robot = iterator.next();
			Point2D position = (Point2D) robot.getTimeline().getLastPoint();
			System.out.println(String.format("(%f, %f)", position.x, position.y));
		}
	}

	@Override
	public String getAuthor() {

		return _author;
	}

	@Override
	public String getPluginShortDescription() {

		return _shortDescription;
	}

	@Override
	public String getPluginLongDescription() {

		return _description;
	}

	@Override
	public String getPluginName() {

		return _pluginName;
	}

	@Override
	public SycamorePanel getPanel_settings() {

		return null;
	}

}
